'use strict';
/*
    Creado por Jorge Cañas Estévez
*/
require('../lib/connectMongoose');
require('../models/Anuncio');
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Anuncio = mongoose.model('Anuncio');

Anuncio.remove({}, function (err, result) {
    if (err) console.log(err);
    console.log('Anuncios borrados');
});

var anuncios = require('./anuncios.json');

Anuncio.insertMany(anuncios.anuncios, function (err, docs) {
    if (err) console.log(err);
    console.log('Anuncios creados', docs);
    process.exit();
});
/*
for ( let anuncio of anuncios.anuncios ) {
    var nuevo = new Anuncio(anuncio);
    nuevo.save(function(err, anuncioGuardado){
        if(err) console.log(err);
        console.log('Anuncio creado', anuncioGuardado);
    });
}
*/