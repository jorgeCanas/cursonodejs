/*
    Creado por Jorge Cañas Estévez
*/
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

/*
Para poder ejecutar un script de node se hace un require de child_process, al que le pasamos la ubicación del
script y lo ejecuta.
*/

var childProcess = require("child_process");
var path = require("path");
var cp = childProcess.fork(path.join(__dirname, "utils/install_db.js"));
cp.on("exit", function (code, signal) {
  console.log("Limpiada BD y creados anuncios base", { code: code, signal: signal });
});
cp.on("error", console.error.bind(console));

var app = express();
require('./lib/connectMongoose');
require('./models/Anuncio');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
//Para poder servir ficheros estáticos se usa express.static al que se le pasa la ruta en la que estarán
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', require('./routes/index'));
app.use('/apiv1/anuncios', require('./routes/apiv1/anuncios'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
