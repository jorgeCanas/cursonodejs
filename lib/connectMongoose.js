'use strict';
/*
    Creado por Jorge Cañas Estévez
    Este script conecta con la base de datos o devuelve un error en caso contrario.
*/
var mongoose = require('mongoose');

var db = mongoose.connection;

//Muestra un mensaje de error si la conexión falla.
db.on('error', function (err) {
    console.log(err);
});

//Abre una conexión con la base de datos, si ya está creada devuelve la conexión
db.once('open', function () {
    console.info('Conectado a mongodb.');
});

//Conecta con la base de datos y la crea en caso de no estar definida
mongoose.connect('mongodb://localhost/cursonodefinal', { useMongoClient: true });
