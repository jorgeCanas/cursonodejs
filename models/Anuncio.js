'use strict';
/*
    Creado por Jorge Cañas Estévez
*/
var mongoose = require('mongoose');
//Esquema del modelo a almacenar en la base de datos. Si se intenta pasar otros parámetros, 
//estos no se guardan (Acción que realizaría MongoDB por defecto)
var anuncioSchema = mongoose.Schema({
    nombre: String,
    venta: Boolean,
    precio: Number,
    foto: String,
    //enum limita las opciones de los tags a guardar, si se intenta insertar otro, falla.
    tags: [{ type: String, enum: ['work', 'lifestyle', 'motor', 'mobile'] }]
});

anuncioSchema.statics.list = function (filter, limit, skip, sort, cb) {
    var query = Anuncio.find(filter);
    query.limit(limit);
    query.skip(skip);
    query.sort(sort);
    query.exec(cb);
};

var Anuncio = mongoose.model('Anuncio', anuncioSchema);
