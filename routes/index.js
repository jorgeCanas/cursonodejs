/*
    Creado por Jorge Cañas Estévez
*/
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Práctica final Curso Node.js' });
});

module.exports = router;
