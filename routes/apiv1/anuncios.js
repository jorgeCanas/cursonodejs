'use strict';
/*
    Creado por Jorge Cañas Estévez
*/
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
// recuperar lista de anuncios
var Anuncio = mongoose.model('Anuncio');
router.get('/', function (req, res, next) {
    //console.log('query', req.query);
    var nombre = req.query.nombre;
    var venta = req.query.venta;
    var precio = req.query.precio;
    var tags = req.query.tag;

    var limit = parseInt(req.query.limit) || null;
    var skip = parseInt(req.query.start) || null;
    var sort = req.query.sort || null;

    var filter = {};
    if (typeof tags !== 'undefined') {
        if (typeof tags === 'string') {
            tags = [tags];
        }
        //Filtra los anuncios que contengan alguno de los tags
        filter.tags = { '$in': tags };
    }

    if (typeof nombre !== 'undefined') {
        //Filtra los anuncios que empiezan por el nombre
        filter.nombre = new RegExp(`^${nombre}`, "i");
    }

    if (typeof venta !== 'undefined') {
        filter.venta = venta;
    }

    if (typeof precio !== 'undefined') {
        const indice = precio.indexOf('-');
        var rango = precio.split('-');
        //console.log('indice del -', indice, 'tamaño', precio.length, 'rango', rango);

        if (indice === -1) {
            //Precio exacto
            precio = parseFloat(precio);

        } else if (indice === 0) {
            //Precio menor o igual
            precio = parseFloat(rango[1]);
            precio = { '$lte': precio };

        } else if (indice === precio.length - 1) {
            //Precio mayor o igual
            precio = parseFloat(precio.split('-')[0]);
            precio = { '$gte': precio };
        } else {
            //Rango de precios
            precio = { '$gte': parseFloat(rango[0]), '$lte': parseFloat(rango[1]) };
        }
        filter.precio = precio;
    }

    //console.log('filter', filter);

    Anuncio.list(filter, limit, skip, sort, function (err, list) {
        if (err) {
            next(err);

            return;
        }
        res.json({ list: list, ok: true });
    });
});

router.get('/tags', function (req, res, next) {
    res.json(Anuncio.schema.path('tags.0').enumValues);
});

// crear un anuncio
router.post('/', function (req, res, next) {
    //console.log(`post ${req.body}`, req.body.nombre);
    var anuncio = new Anuncio(req.body);
    if (anuncio.nombre) {
        anuncio.save(function (err, anuncioGuardado) {
            if (err) {
                return next(err);
            }

            return res.json({ anuncio: anuncioGuardado, ok: true });
        });
    } else {
        res.json({ mensaje: 'Debe proporcionar un nombre', ok: false });
    }
});

module.exports = router;
